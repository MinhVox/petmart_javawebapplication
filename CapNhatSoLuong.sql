﻿/* cập nhật hàng trong kho sau khi đặt hàng hoặc cập nhật */
CREATE TRIGGER trg_DatHang ON Invoice_Product_Detail AFTER INSERT 
AS 
BEGIN
	UPDATE Product
	SET Quantity = Product.Quantity - (
		SELECT Quantity
		FROM inserted
		WHERE IDpro = Product.IDpro
	)
	FROM Product
	JOIN inserted ON Product.IDpro= inserted.IDpro
END
GO
/* cập nhật hàng trong kho sau khi cập nhật đặt hàng */
CREATE TRIGGER trg_CapNhatDatHang on Invoice_Product_Detail after update
 AS
BEGIN
   UPDATE Product SET Quantity = Product.Quantity -
	   (SELECT Quantity FROM inserted WHERE IDpro = Product.IDpro) +
	   (SELECT Quantity FROM deleted WHERE IDpro = Product.IDpro)
   FROM Product
   JOIN deleted ON Product.IDpro = deleted.IDpro
end
GO
/* cập nhật hàng trong kho sau khi hủy đặt hàng */
create TRIGGER trg_HuyDatHang ON Invoice_Product_Detail FOR DELETE 
AS 
BEGIN
	UPDATE Product
	SET Quantity = Product.Quantity + (SELECT Quantity FROM deleted WHERE IDpro = Product.IDpro)
	FROM Product
	JOIN deleted ON Product.IDpro = deleted.IDpro
END