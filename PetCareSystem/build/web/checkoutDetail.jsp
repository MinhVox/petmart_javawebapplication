<%@page import="minhvq.errorObjects.RegistrationErrorObj"%>
<%@page import="minhvq.dtos.AccessoryDTO"%>
<%@page import="minhvq.dtos.ShoppingCartAccessory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <style>
        .left{
            width: 40%;
            float: left;
            border-top: 1px solid #777777;
            padding-left: 20px;
            padding-right: 20px;
        }
        .right{
            width: 40%;
            padding-left: 20px;
            border: 2px solid #777777;
            float: left;
            padding-right: 10px;
            margin-left: 50px;
        }
        .form-group{
            width: 50%;

        }

    </style>
    <body>
        <div class="row">
            <div class="col">
                <img src="IMG/logo-petmart.png" style="float: right ">
            </div>
            <div class="col-md-auto">
                <form class="form-inline" action="search">
                    <input type="text" class="form-control" id="search" placeholder="Find product" name="search">
                    <button type="submit" class="btn btn-outline-primary" id="btnSearch">Search</button>
                </form>
            </div>
            <div class="col">
                <%
                    String name = (String) session.getAttribute("NAME");
                    if (name != null) {
                %>
                <div class="dropdown" style="margin-left: 100px">
                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                        <span> Hello,<br> <%= name%> </span>
                    </button>                   
                    <div class="dropdown-menu">
                        <a href="viewListOrder.jsp"><button class="dropdown-item">View Order</button></a>
                        <form action="MainController" method="POST">
                            <button class="dropdown-item" value="logout" name="action">Logout</button>
                        </form>
                    </div>
                </div>
                <%
                } else {
                %>
                <form style="float: right;margin-top:25px;margin-right:50px ">
                    <a href="login.jsp"><button type="button" class="btn btn-danger" id="btnLogin">Login</button></a>
                </form>
                <%
                    }
                %>
            </div>
            <div class="col" >
                <%
                    ShoppingCartAccessory cart = (ShoppingCartAccessory) session.getAttribute("cart");
                    int value = 0;
                    if (cart != null) {
                        value = cart.getValues();
                    }
                %>
                <a href="#" id="cart"><i class="fa fa-cart-arrow-down" aria-hidden="true"
                                         style="font-size: 30px;padding-left:100px;margin-top: 17px"></i>Cart
                    <span class="badge"><%= value%></span></a>
            </div>
        </div>
        <hr>
        <div class="row" style="background-color: #F1F1F1">
            <div class="col-1"></div>
            <div class="col" >
                <div class="dropdown">
                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                        <b style="font-size: 20px">Shopping Online</b>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Shopping for cat</a>
                        <a class="dropdown-item" href="#">Shopping for dog</a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="dropdown">
                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                        <b style="font-size: 20px">Pet service</b>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Cat dog grooming service</a>
                        <a class="dropdown-item" href="#">Spa bath service for cats and dogs</a>
                        <a class="dropdown-item" href="#">Service of keeping cats and dogs</a>
                    </div>
                </div>
            </div>
            <div class="col"><button type="button" class="btn ">
                    <b style="font-size: 20px">News</b>
                </button></div>
            <div class="col">
                <div>
                    <div class="icon" id="i1">
                        <i class="fa fa-facebook" style="font-size: 25px"></i><br><br>
                        <i class="fa fa-facebook" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i2">
                        <i class="	fa fa-google" style="font-size: 25px"></i><br><br>
                        <i class="	fa fa-google" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i3">
                        <i class="fa fa-skype" style="font-size: 25px"></i><br><br>
                        <i class="fa fa-skype" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i4">
                        <i class="fa fa-vimeo" style="font-size: 25px"></i><br><br>
                        <i class="fa fa-vimeo" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i5">
                        <i class="	fa fa-twitter" style="font-size: 25px"></i><br><br>
                        <i class="	fa fa-twitter" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i6">
                        <i class="	fa fa-git" style="font-size: 25px"></i><br><br>
                        <i class="	fa fa-git" style="font-size: 25px;color:white"></i>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <h2 style="text-align: center">SHOPPING CART > CHECKOUT DETAIL</h2>
        <div style="padding-left: 50px;">
            <form action="MainController" method="POST">
                <div class="left">
                    <h4><b>Payment and Delivery</b></h4>
                    <%
                        RegistrationErrorObj err = (RegistrationErrorObj) request.getAttribute("INVALID");
                    %>


                    <div class="form-group" >
                        <input type="text" class="form-control"  placeholder="Address" name="txtAddress">
                        <%
                            if (request.getAttribute("INVALID") != null) {
                                if (err.getErrAddress() != null) {
                        %>
                        <font color="red">
                        <%= err.getErrAddress()%>
                        </font>
                        <%
                                }
                            }
                        %>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control"  placeholder="Phone" name="txtPhone">
                        <%
                            if (request.getAttribute("INVALID") != null) {
                                if (err.getErrPhone() != null) {
                        %>
                        <font color="red">
                        <%= err.getErrPhone()%>
                        </font>
                        <%
                                }
                            }
                        %>
                    </div><br>
                   
                    <a href="viewCart.jsp"><button class="btn-outline-dark">← Continue shopping</button></a>
                </div>

                <div class="right">
                    <h4><b>Your Order</b></h4> 
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Product</th>
                                <th scope="col">Sum</th>
                            </tr>
                        <tbody>
                            <%
                                for (AccessoryDTO dto : cart.getCart().values()) {
                            %>
                            <tr>
                                <td><%= dto.getName()%> x <%= dto.getQuanity()%></td>
                                <td><%= dto.getPrice() * dto.getQuanity()%></td>
                            </tr>
                            <%
                                }
                            %>
                            <tr>
                                <td style="color: #777777">Provisional</td>
                                <td style="color: #00A9F0"><%= cart.getTotal()%>₫</td>

                            </tr>
                            <tr>
                                <td style="color: #777777">Total</td>
                                <td style="color: #00A9F0"><%= cart.getTotal()%>₫</td>
                            </tr>
                        </tbody>
                        </thead>

                    </table>

                    <button class="btn btn-outline-primary" type="submit"  name="action" value="Add Invoice">
                        Order
                    </button>

                </div>
            </form>
        </div>
    </body>
