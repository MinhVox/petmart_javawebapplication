<%-- 
    Document   : admin
    Created on : Jun 27, 2019, 5:47:55 PM
    Author     : Quang Minh
--%>

<%@page import="minhvq.dtos.OrderDTO"%>
<%@page import="minhvq.dtos.AccessoryDTO"%>
<%@page import="minhvq.dtos.RegistrationDTO"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="row">
            <div class="col">
                <img src="IMG/logo-petmart.png" style="float: right ">
            </div>
            <div class="col-md-auto">
                <form class="form-inline" action="search">
                    <input type="text" class="form-control" id="search" placeholder="Find product" name="search">
                    <button type="submit" class="btn btn-outline-primary" id="btnSearch">Search</button>
                </form>
            </div>
            <div class="col">
                <%
                    String name = (String) session.getAttribute("NAME");
                    if (name != null) {
                %>
                <div class="dropdown" style="margin-left: 100px">
                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                        <span> Hello,<br> <%= name%> </span>
                    </button>                   
                    <div class="dropdown-menu">
      
                        <form action="MainController" method="POST">
                            <button class="dropdown-item" value="logout" name="action">Logout</button>
                        </form>
                    </div>
                </div>
                <%
                } else {
                %>
                <form style="float: right;margin-top:25px;margin-right:50px ">
                    <a href="login.jsp"><button type="button" class="btn btn-danger" id="btnLogin">Login</button></a>
                </form>
                <%
                    }
                %>
            </div>
        </div>
        <div class="row" style="background-color: #F1F1F1">
            <div class="col-1"></div>

            <div class="col" >
                <form action="MainController" method="POST">
                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                        <b style="font-size: 20px">Account </b>
                    </button>
                    <div class="dropdown-menu">
                        <button type="submit" class="dropdown-item" name="action" value="See Account">
                            User
                        </button>
                        <button type="submit" class="dropdown-item" name="action" value="See Admin">
                           Admin
                        </button>
                    </div>
                </form>
            </div>
            <div class="col" >
                <form action="MainController" method="POST">
                    <button class="btn " name="action" value="See Product">
                        <b style="font-size: 20px">Accessory</b>
                    </button>
                </form>
            </div>
            <div class="col">
                <form action="MainController" method="POST">
                    <button type="submit" class="btn">
                        <b style="font-size: 20px">Service </b>
                    </button>
                </form>
            </div>
            <div class="col">
                <form action="MainController" method="POST">
                    <button type="submit" class="btn" name="action" value="See Invoice">
                        <b style="font-size: 20px">Order</b>
                    </button>
                </form>
            </div>
            <div class="col">
                <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                    <b style="font-size: 20px">Add new </b>
                </button>
                <div class="dropdown-menu">
                    <a href="admin_InsertProduct.jsp"><button class="dropdown-item">Add new Product</button> </a>
                    <a href="admin_InsertAdmin.jsp"><button class="dropdown-item">Add new Admin</button> </a>
                </div>
            </div>
        </div>
        <div>
            <%
                List<RegistrationDTO> listAccount = (List<RegistrationDTO>) request.getAttribute("ACC");
                int count1 = 0;
                if (listAccount != null) {
            %>
            <br><br><br>
            <h2 style="text-align: center">List User</h2>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Username</th>
                        <th scope="col">Fullname</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (RegistrationDTO dto : listAccount) {
                            count1++;
                    %>
                    <tr>
                        <th scope="row"><%= count1%></th>
                        <td><%= dto.getUsername()%></td>
                        <td><%= dto.getFullname()%></td>
                        <td><%= dto.getSex()%></td>
                        <td>
                            <form action="MainController" method="POST">
                                <input type="hidden" value="<%= dto.getUsername()%>-user" name="id">
                                <button type="submit" class="btn btn-outline-dark" value="DeleteAcc" name="action">
                                    <i class="fa fa-remove"></i>
                                </button>   
                            </form>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
            <%
                }
            %>
        </div>  
        <div>
            <%
                List<AccessoryDTO> listPro = (List<AccessoryDTO>) request.getAttribute("PRO");
                int count2 = 0;
                if (listPro != null) {
            %>
            <br><br><br>
            <h2 style="text-align: center">List Product</h2>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">ID</th>
                        <th scope="col">IMG</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Type</th>
                        <th scope="col">Delete</th>
                        <th scope="col">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (AccessoryDTO dto : listPro) {
                            count2++;
                    %>
                    <tr>
                        <th scope="row"><%= count2%></th>
                        <td><%= dto.getId()%></td>
                        <td><img src="IMG/<%= dto.getImg()%>" style="width: 75px;height: 75px"></td>
                        <td><%= dto.getName()%></td>
                        <td><%= dto.getPrice()%></td>
                        <td><%= dto.getQuanity()%></td>
                        <td><%= dto.getType()%></td>
                        <td>
                            <form action="MainController" method="POST">
                                <input type="hidden" value="<%= dto.getId()%>" name="id">
                                <button type="submit" class="btn btn-outline-dark" value="DeletePro" name="action">
                                    <i class="fa fa-remove"></i>
                                </button>   
                            </form>
                        </td>
                        <td>
                            <form action="MainController" method="POST">
                                <input type="hidden" value="<%= dto.getId()%>" name="id">
                                <button type="submit" class="btn btn-outline-dark" value="EditPro" name="action">
                                    <i class="fa fa-edit"></i>
                                </button>   
                            </form>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
            <%
                }
            %>
        </div>  
        
        <%
                List<RegistrationDTO> listadmin = (List<RegistrationDTO>) request.getAttribute("ADM");
                int count3 = 0;
                if (listadmin != null) {
            %>
            <br><br><br>
            <h2 style="text-align: center">List Admin</h2>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Username</th>
                        <th scope="col">Fullname</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (RegistrationDTO dto : listadmin) {
                            count3++;
                    %>
                    <tr>
                        <th scope="row"><%= count3%></th>
                        <td><%= dto.getUsername()%></td>
                        <td><%= dto.getFullname()%></td>
                        <td><%= dto.getSex()%></td>
                        <td>
                            <form action="MainController" method="POST">
                                <input type="hidden" value="<%= dto.getUsername()%>-admin" name="id">
                                <button type="submit" class="btn btn-outline-dark" value="DeleteAcc" name="action">
                                    <i class="fa fa-remove"></i>
                                </button>   
                            </form>
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
            <%
                }
            %>
        </div>  
        
        <%
                List<OrderDTO> listOrder = (List<OrderDTO>) request.getAttribute("ORD");
                if (listOrder != null) {
            %>
            <br><br><br>
            <h2 style="text-align: center">List Order</h2>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Fullname</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Address</th>
                        <th scope="col">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        for (OrderDTO dto : listOrder) {
                    %>
                    <tr>
                        <td><a><%= dto.getId() %></a></td>
                        <td><%= dto.getDate() %></td>
                        <td><%= dto.getName() %></td>
                        <td><%= dto.getPhone() %></td>
                        <td><%= dto.getAddress() %></td>
                        <td><%= dto.getTotal() %>đ</td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
            <%
                }
            %>
        </div>  
    </body>
</html>
