<%-- 
    Document   : login
    Created on : Jun 24, 2019, 7:22:17 AM
    Author     : Quang Minh
--%>

<%@page import="minhvq.errorObjects.RegistrationErrorObj"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <style>
        body{
            margin: 0;
            padding: 0;
        }
        #bg{
            position: absolute;
            z-index: -1;
        }
        #form{
            position: absolute;
            right: 450px;
            bottom: 200px;
            z-index: 1;
        }
    </style>
    <body>
        <%
            RegistrationErrorObj err = (RegistrationErrorObj) request.getAttribute("INVALID");
        %>
        <img src="IMG/DvsC.jpg" width="100%" height="800px" id="bg">
        <div style="width: 400px; height: 500px;top: 100px; align-items: center;background-color: rgba(178, 174, 174, 0.6);" id="form" >
            <img src="IMG/logo-petmart.png" class="mx-auto d-block"><br>
            <img src="IMG/2.png" width="50px" height="50px" class="mx-auto d-block"><br>
            <form action="MainController" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control"  placeholder="UserName" name="txtUsername">
                    <%
                        if (request.getAttribute("INVALID") != null) {
                            if (err.getErrUsername() != null) {

                    %>
                    <font color="red">
                    <%= err.getErrUsername()%>
                    </font>
                    <%
                            }
                        }
                    %>
                </div><br>
                <div class="form-group">
                    <input type="password" class="form-control"  placeholder="Password" name="txtPassword">
                    <%
                        if (request.getAttribute("INVALID") != null) {
                            if (err.getErrPassword() != null) {

                    %>
                    <font color="red">
                    <%= err.getErrPassword()%>
                    </font>
                    <%
                            }
                        }
                    %>
                </div>
                 <%
                        if (request.getAttribute("INVALID") != null) {
                            if (err.getErrLogin() != null) {

                    %>
                    <font color="red">
                    <%= err.getErrLogin()%>
                    </font>
                    <%
                            }
                        }
                    %>
                <button type="submit" class="btn btn-primary mx-auto d-block" name="action" value="Login">
                    <b style="font-size: 20px">Login</b>
                </button>
                <hr>
            </form>
            <a href="register.jsp"><button type="button" class="btn btn-primary mx-auto d-block" >
                    <b style="font-size: 20px">Register</b>
                </button></a>

        </div>  
    </body>
</html>
