<%-- 
    Document   : insertPro
    Created on : Jul 14, 2019, 11:52:55 AM
    Author     : Quang Minh
--%>

<%@page import="minhvq.errorObjects.AccessoryErrorObj"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <h1>Add new Accessory</h1>
        <%
            AccessoryErrorObj errObj = (AccessoryErrorObj) request.getAttribute("INVALID");
        %>
        <form action="MainController" method="POST">
            Accessory  ID:<input type="text" name="txtID" > 
            <%
                if (request.getAttribute("INVALID") != null) {
                    if (errObj.getIdErr() != null) {
            %>
            <font color="red">
            <%= errObj.getNameErr()%>
            </font>
            <%
                    }
                }
            %><br>
            Accessory Name: <input type="text" name="txtName" > 
            <%
                if (request.getAttribute("INVALID") != null) {
                    if (errObj.getNameErr() != null) {
            %>
            <font color="red">
            <%= errObj.getNameErr()%>
            </font>
            <%
                    }
                }
            %>
            <br>
            Price: <input type="text" name="txtPrice" > 
            <%
                if (request.getAttribute("INVALID") != null) {
                    if (errObj.getPriceErr() != null) {
            %>
            <font color="red">
            <%= errObj.getPriceErr()%>
            </font>
            <%
                    }
                }
            %>
            <br>
            Quantity: <input type="text" name="txtQuantity" > 
            <%
                if (request.getAttribute("INVALID") != null) {
                    if (errObj.getQuantityErr() != null) {
            %>
            <font color="red">
            <%= errObj.getQuantityErr()%>
            </font>
            <%
                    }
                }
            %>
            <br>
            Type:<div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" checked="true" name="type" value="1">Dog
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="type" value="2">Cat
                </label>
            </div>
            <div class="form-group">
                <label>Image: </label>
                <input type="file" name="fileImg">
            </div>
            <input type="submit" name="action" value="InsertPro">
        </form>
    </body>
</html>
