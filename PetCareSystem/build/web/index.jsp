<%-- 
    Document   : index
    Created on : May 27, 2019, 10:54:07 AM
    Author     : Quang Minh
--%>

<%@page import="minhvq.dtos.ShoppingCartAccessory"%>
<%@page import="minhvq.dtos.AccessoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="minhvq.daos.AccessoryDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <style>


    </style>

    <body>
        <div class="row">
            <div class="col">
                <a href="index.jsp"><img src="IMG/logo-petmart.png" style="float: right "></a>
            </div>
            <div class="col-md-auto">
                <form class="form-inline" action="search">
                    <input type="text" class="form-control" id="search" placeholder="Find product" name="search">
                    <button type="submit" class="btn btn-outline-primary" id="btnSearch">Search</button>
                </form>
            </div>
            <div class="col">
                <%
                    String name = (String) session.getAttribute("NAME");
                    if (name != null) {
                %>
                <div class="dropdown" style="margin-left: 100px">
                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                        <span> Hello,<br> <%= name%> </span>
                    </button>                   
                    <div class="dropdown-menu">
                        <a href="viewListOrder.jsp"><button class="dropdown-item">View Order</button></a>
                        <form action="MainController" method="POST">
                            <button class="dropdown-item" value="logout" name="action">Logout</button>
                        </form>
                    </div>
                </div>
                <%
                } else {
                %>
                <form style="float: right;margin-top:25px;margin-right:50px ">
                    <a href="login.jsp"><button type="button" class="btn btn-danger" id="btnLogin">Login</button></a>
                </form>
                <%
                    }
                %>
            </div>
            <div class="col" >
                <%
                    ShoppingCartAccessory cart = (ShoppingCartAccessory) session.getAttribute("cart");
                    int value = 0;
                    if (cart != null) {
                        value = cart.getValues();
                    }
                %>
                <a href="#" id="cart"><i class="fa fa-cart-arrow-down" aria-hidden="true"
                                         style="font-size: 30px;padding-left:100px;margin-top: 17px"></i>Cart
                    <span class="badge"><%= value%></span></a>

                <div class="container">
                    <div class="shopping-cart">
                        <%
                            if (cart == null || cart.getTotal() == 0) {
                        %>
                        Nothing in cart
                        <%
                        } else {
                            for (AccessoryDTO dto : cart.getCart().values()) {
                        %>
                        <ul class="shopping-cart-items">
                            <li class="clearfix">
                                <img src="IMG/<%= dto.getImg()%>" width="50px" height="50px"/>
                                <span class="item-name"><%= dto.getName()%></span><br>
                                <span class="item-price"><%= dto.getPrice()%>₫</span>
                                <span class="item-quantity">x <%= dto.getQuanity()%></span>
                                <form action="MainController" method="POST">
                                    <input type="hidden" value="<%= dto.getId()%>-index.jsp" name="idRemove">
                                    <button type="submit" class="btn btn-outline-dark" value="Remove Item" name="action">
                                        <i class="fa fa-remove"></i>
                                    </button>                                          
                                </form>
                            </li>
                        </ul>
                        <%
                                }
                            }
                        %>
                        <%
                            if (cart == null || cart.getTotal() == 0) {
                        %>
                        <%
                        } else {

                        %>
                        <div class="dropdown-divider"></div>
                        <span style="text-align: center;color: #777777">Total:</span>
                        <span style="text-align: center;color: #68A3FE"><%= cart.getTotal()%> ₫</span>
                        <div class="dropdown-divider"></div>
                        <a href="viewCart.jsp" class="button" style="background-color: #292929">View Cart</a>
                        <a href="checkoutDetail.jsp" class="button" style="background-color: #3584FE">Checkout</a>
                        <%
                            }
                        %>
                    </div> <!--end shopping-cart -->
                </div>
            </div>
        </div>
        <hr>
        <div class="row" style="background-color: #F1F1F1">
            <div class="col-1"></div>
            <div class="col" >
                <div class="dropdown">
                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                        <b style="font-size: 20px">Shopping Online</b>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Shopping for Dogs</a>
                        <a class="dropdown-item" href="#">Shopping for Cats</a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="dropdown">
                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                        <b style="font-size: 20px">Pet service</b>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Cat dog grooming service</a>
                        <a class="dropdown-item" href="#">Spa bath service for cats and dogs</a>
                        <a class="dropdown-item" href="#">Service of keeping cats and dogs</a>
                    </div>
                    </div>
            </div>
            <div class="col"><button type="button" class="btn ">
                    <b style="font-size: 20px">Tin tức</b>
                </button></div>
            <div class="col">
                <div>
                    <div class="icon" id="i1">
                        <i class="fa fa-facebook" style="font-size: 25px"></i><br><br>
                        <i class="fa fa-facebook" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i2">
                        <i class="	fa fa-google" style="font-size: 25px"></i><br><br>
                        <i class="	fa fa-google" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i3">
                        <i class="fa fa-skype" style="font-size: 25px"></i><br><br>
                        <i class="fa fa-skype" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i4">
                        <i class="fa fa-vimeo" style="font-size: 25px"></i><br><br>
                        <i class="fa fa-vimeo" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i5">
                        <i class="	fa fa-twitter" style="font-size: 25px"></i><br><br>
                        <i class="	fa fa-twitter" style="font-size: 25px;color:white"></i>
                    </div>
                    <div class="icon" id="i6">
                        <i class="	fa fa-git" style="font-size: 25px"></i><br><br>
                        <i class="	fa fa-git" style="font-size: 25px;color:white"></i>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-1"></div>
            <div class="col">
                <h3>Hệ thống cửa hàng thú cưng 𝗣𝗘𝗧 𝗠𝗔𝗥𝗧</h3>
                <p>𝗣𝗘𝗧 𝗠𝗔𝗥𝗧 là hệ thống pet shop tại Hà Nội, Đà Nẵng và TP.HCM cung cấp đồ dùng, quần áo, thức ăn, sữa tắm, chuồng, vòng cổ xích và các phụ kiện chó mèo hàng đầu tại Việt Nam. Địa chỉ tắm spa, chăm sóc, cắt tỉa lông và trông giữ thú cưng chuyên nghiệp. Với chất lượng dịch vụ tốt nhất luôn được khách hàng tin tưởng và là điểm đến tuyệt vời dành cho thú cưng.</p>
            </div>
            <div class="col">
                <button type="button" class="btn btn-primary d-block">
                    Tổng dài tư vấn: 19002100
                </button>
                <p>Thời gian làm việc: 9:00 - 22:00 tất cả các ngày trong tuần</p>
            </div>
        </div>
        <div style="padding-left: 100px;padding-right: 100px;">
            <%
                AccessoryDAO dao = new AccessoryDAO();
                List<AccessoryDTO> result = dao.listAccessories();
                if (result != null) {
                    if (result.size() > 0) {
                        for (AccessoryDTO dto : result) {

            %>
            <div class="card" style="width: 200px;float: left;margin-left:17px;height: 300px;margin-bottom: 30px;">
                <img src="IMG/<%= dto.getImg()%>" class="card-img-top" width="200px" height="300px" style="float: left">
                <div class="addToCart" title="Add to Cart" >    
                    <form action="MainController" method="POST">
                        <input type="hidden" name="id" value="<%= dto.getId()%>-index.jsp"/>
                        <button type="submit" class="btn btn-outline-dark" name="action" value="Add To Cart">
                            <i class="fa fa-cart-arrow-down" style="font-size: 18px;"></i>
                        </button>
                    </form>
                </div>
                <div class="card-body">
                    <a href="MainController?action=INFO&id=<%= dto.getId()%>" class="link-product" ><%= dto.getName()%></a><br>
                    <font color="blue">
                    <%= dto.getPrice()%>₫
                    </font>
                </div>
            </div>    
            <%
                        }
                    }
                }
            %>
        </div>
    </body>

</html>
