<%-- 
    Document   : admin_UpdateProduct
    Created on : Jul 13, 2019, 12:46:43 PM
    Author     : Quang Minh
--%>

<%@page import="minhvq.errorObjects.AccessoryErrorObj"%>
<%@page import="minhvq.errorObjects.RegistrationErrorObj"%>
<%@page import="minhvq.dtos.AccessoryDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Update Accessory</h1>
        <%
            AccessoryDTO dto = (AccessoryDTO) request.getAttribute("DTO");
            AccessoryErrorObj errObj = (AccessoryErrorObj) request.getAttribute("INVALID");
        %>
        <form>
            Accessory  ID:<input type="text" name="txtID" value="<%= dto.getId()%>" readonly="true"> <br>

            Accessory Name: <input type="text" name="txtName" value="<%= dto.getName()%>" > 
            <%
                if (request.getAttribute("INVALID") != null) {
                    if (errObj.getNameErr() != null) {
            %>
            <font color="red">
            <%= errObj.getNameErr()%>
            </font>
            <%
                }}
            %>
            <br>
            Price: <input type="text" name="txtPrice" value="<%= dto.getPrice()%>" > 
            <%
                if (request.getAttribute("INVALID") != null) {
                    if (errObj.getPriceErr() != null) {
            %>
            <font color="red">
            <%= errObj.getPriceErr()%>
            </font>
            <%
                }}
            %>
            <br>
            Quantity: <input type="text" name="txtQuantity" value="<%= dto.getQuanity()%>" > 
            <%
                if (request.getAttribute("INVALID") != null) {
                    if (errObj.getQuantityErr() != null) {
            %>
            <font color="red">
            <%= errObj.getQuantityErr()%>
            </font>
            <%
                }}
            %>
            <br>
            Type:<div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" <% if(dto.getTypeID() == 1){ %> checked="true"<% } %> name="type" value="1">Dog
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" <% if(dto.getTypeID() == 2){ %> checked="true"<% } %> name="type" value="2">Cat
                </label>
            </div> <br>
            <input type="submit" name="action" value="UpdatePro">
        </form>
    </body>
</html>
