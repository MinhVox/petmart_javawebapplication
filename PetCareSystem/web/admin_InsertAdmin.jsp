<%-- 
    Document   : admin_InsertAdmin
    Created on : Jul 14, 2019, 3:07:55 PM
    Author     : Quang Minh
--%>

<%@page import="minhvq.errorObjects.RegistrationErrorObj"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <h1>Add new Admin</h1>
        <%
            RegistrationErrorObj err =  (RegistrationErrorObj) request.getAttribute("INVALID");
        %>
        <div style="width: 30%">
        <form action="MainController" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control" id="usr" placeholder="UserName" name="txtUsername" width="30px">
                    <%
                        if (request.getAttribute("INVALID") != null) {
                            if (err.getErrUsername() != null) {

                    %>
                    <font color="red">
                    <%= err.getErrUsername()%>
                    </font>
                    <%
                            }
                        }
                    %>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="usr" placeholder="FullName" name="txtFullname">
                    <%
                        if (request.getAttribute("INVALID") != null) {
                            if (err.getErrFullname() != null) {

                    %>
                    <font color="red">
                    <%= err.getErrFullname()%>
                    </font>
                    <%
                            }
                        }
                    %>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="pwd" placeholder="Password" name="txtPassword">
                    <%
                        if (request.getAttribute("INVALID") != null) {
                            if (err.getErrPassword() != null) {

                    %>
                    <font color="red">
                    <%= err.getErrPassword()%>
                    </font>
                    <%
                            }
                        }
                    %>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="pwd" placeholder="Confirm Password" name="txtConfirm">
                    <%
                        if (request.getAttribute("INVALID") != null) {
                            if (err.getErrConfirm() != null) {

                    %>
                    <font color="red">
                    <%= err.getErrConfirm()%>
                    </font>
                    <%
                            }
                        }
                    %>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="gender" checked="true" value="Male">Male
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="gender" value="Female">Female
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="gender" value="Other">Other
                    </label>
                </div>
                <button type="submit" class="btn btn-primary mx-auto d-block" name="action" value="Add Admin" >
                        <b style="font-size: 20px">Create</b>
                    </button>

            </form>
        </div>
    </body>
</html>
