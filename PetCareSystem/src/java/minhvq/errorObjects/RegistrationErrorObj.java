/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.errorObjects;

/**
 *
 * @author Quang Minh
 */
public class RegistrationErrorObj {
    private String errUsername, errPassword,errFullname,errLogin,errConfirm,errPhone,errAddress;

    public String getErrPhone() {
        return errPhone;
    }

    public void setErrPhone(String errPhone) {
        this.errPhone = errPhone;
    }

    public String getErrAddress() {
        return errAddress;
    }

    public void setErrAddress(String errAddress) {
        this.errAddress = errAddress;
    }

    public String getErrConfirm() {
        return errConfirm;
    }

    public void setErrConfirm(String errConfirm) {
        this.errConfirm = errConfirm;
    }

    public String getErrLogin() {
        return errLogin;
    }

    public void setErrLogin(String errLogin) {
        this.errLogin = errLogin;
    }

    public RegistrationErrorObj() {
    }

    public String getErrUsername() {
        return errUsername;
    }

    public void setErrUsername(String errUsername) {
        this.errUsername = errUsername;
    }

    public String getErrPassword() {
        return errPassword;
    }

    public void setErrPassword(String errPassword) {
        this.errPassword = errPassword;
    }

    public String getErrFullname() {
        return errFullname;
    }

    public void setErrFullname(String errFullname) {
        this.errFullname = errFullname;
    }
    
    
}
