/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import minhvq.daos.AccessoryDAO;
import minhvq.dtos.AccessoryDTO;
import minhvq.dtos.ShoppingCartAccessory;

/**
 *
 * @author Quang Minh
 */
public class AddToCartController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String AccessINFO = request.getParameter("id");
            String[] tmp = AccessINFO.split("-");
            String id = tmp[0];
            String url = tmp[1];
        try {
            
            HttpSession session = request.getSession();
            ShoppingCartAccessory shoppingCart = (ShoppingCartAccessory) session.getAttribute("cart");
            if(shoppingCart ==null){
                shoppingCart = new ShoppingCartAccessory("GUEST");
            }
            AccessoryDAO dao = new AccessoryDAO();
            AccessoryDTO dto = dao.getAccessory(id);
            AccessoryDTO item = new AccessoryDTO();
            item.setId(id);
            item.setName(dto.getName());
            item.setPrice(dto.getPrice());
            item.setImg(dto.getImg());
            item.setQuanity(1);
            shoppingCart.addCart(item);
            session.setAttribute("cart", shoppingCart);
        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
