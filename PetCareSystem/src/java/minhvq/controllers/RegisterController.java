/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import minhvq.daos.RegistrationDAO;
import minhvq.dtos.RegistrationDTO;
import minhvq.errorObjects.RegistrationErrorObj;

/**
 *
 * @author Quang Minh
 */
public class RegisterController extends HttpServlet {
    private static final String ERROR = "error.jsp";
    private static final String SUCCESS = "index.jsp";
    private static final String INVALID = "register.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String username = request.getParameter("txtUsername");
            String password = request.getParameter("txtPassword");
            String fullname = request.getParameter("txtFullname");
            String confirm = request.getParameter("txtConfirm");
            String sex = request.getParameter("gender");
            HttpSession session = request.getSession();
            boolean valid = true;
            RegistrationErrorObj err = new RegistrationErrorObj();
            if(username.length() == 0){
                valid = false;
                err.setErrUsername("Username can't be blank");
            }
            if(password.length() == 0){
                valid = false;
                err.setErrPassword("Password can't be blank");
            }
            if(!confirm.equals(password)){
                valid = false;
                err.setErrConfirm("Confirm must match password");
            }
            if(fullname.length() == 0){
                valid = false;
                err.setErrFullname("Fullname can't be blank");
            }
            if(valid){
                RegistrationDAO dao = new RegistrationDAO();
                RegistrationDTO dto = new RegistrationDTO(username, fullname, password, sex);
                if(dao.registerAccount(dto)){
                    String name = dao.getFullname(username);
                    session.setAttribute("NAME", name);
                    session.setAttribute("USERNAME", username);
                    url = SUCCESS;
                }else{
                    url = ERROR;
                }
            }else{
                request.setAttribute("INVALID", err);
                url = INVALID;                
            }
        } catch (Exception e) {
            log("Errror at RegisterController : " + e.getMessage());
            if(e.getMessage().contains("duplicate")){
                url = INVALID;
                RegistrationErrorObj err = new RegistrationErrorObj();
                err.setErrUsername("Username is already existed");
                request.setAttribute("INVALID", err);
            }   
        }finally{
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
