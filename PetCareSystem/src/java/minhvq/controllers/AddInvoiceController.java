/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import minhvq.daos.AccessoryDAO;
import minhvq.daos.OrderDAO;
import minhvq.dtos.AccessoryDTO;
import minhvq.dtos.ShoppingCartAccessory;
import minhvq.errorObjects.RegistrationErrorObj;

/**
 *
 * @author Quang Minh
 */
public class AddInvoiceController extends HttpServlet {
    private static final String ERROR = "error.jsp";
    private static final String SUCCESS = "orderComplete.jsp";
    private static final String INVALID = "checkoutDetail.jsp";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            String username = (String) session.getAttribute("USERNAME");
            String phone = request.getParameter("txtPhone");
            String address = request.getParameter("txtAddress");
            boolean valid = true;
            RegistrationErrorObj err = new RegistrationErrorObj();
            if (phone.length() == 0) {
                valid = false;
                err.setErrPhone("Phone can't be blank");
            } else if (!phone.matches("^[0-9]*$")) {
                valid = false;
                err.setErrPhone("Phone is a number");
            }
            if (address.length() == 0) {
                valid = false;
                err.setErrAddress("Address can't be blank");
            }
            if (username == null) {
                url = "login.jsp";
            } else {
                if (valid) {
                    OrderDAO dao = new OrderDAO();
                    System.out.println(username + phone + address);
                    int id = dao.insertInvoice(username, address, phone);
                    if (id > 0) {
                        ShoppingCartAccessory cart = (ShoppingCartAccessory) session.getAttribute("cart");
                        for (AccessoryDTO dto : cart.getCart().values()) {
                            boolean result = dao.insertDetail(id, dto.getId(), dto.getQuanity(), dto.getPrice());
                            if (result == false) {
                                url = ERROR;
                            }
                        }
                        url = SUCCESS;
                        session.setAttribute("ID order", id);
                    } else {
                        url = ERROR;
                    }
                }else{
                    request.setAttribute("INVALID", err);
                    url = INVALID;
            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
