/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import minhvq.daos.RegistrationDAO;
import minhvq.errorObjects.RegistrationErrorObj;

/**
 *
 * @author Quang Minh
 */
public class LoginController extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String ADMIN = "admin.jsp";
    private static final String USER = "index.jsp";
    private static final String INVALID = "login.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String username = request.getParameter("txtUsername");
            String password = request.getParameter("txtPassword");
            HttpSession session = request.getSession();
            boolean valid = true;
            RegistrationErrorObj err = new RegistrationErrorObj();
            if (username.length() == 0) {
                valid = false;
                err.setErrUsername("Username can't be blank");
            }
            if (password.length() == 0) {
                valid = false;
                err.setErrPassword("Password can't be blank");
            }
            if (valid) {
                RegistrationDAO dao = new RegistrationDAO();
                String role = dao.checkLogin(username, password);
                System.out.println(role);
                if (role.equals("failed")) {
                    err.setErrLogin("Invalid username or password");
                    request.setAttribute("INVALID", err);
                    url =INVALID;
                } else if (role.equals("admin")) {
                    String name = dao.getFullname(username);
                    session.setAttribute("NAME", name);
                    url = ADMIN;
                } else if (role.equals("user")) {
                    String name = dao.getFullname(username);
                    session.setAttribute("NAME", name);
                    session.setAttribute("USERNAME", username);
                    url = USER;
                } else {
                    session.setAttribute("ERROR", "Your Role is not supported");
                }
            }else{
                url = INVALID;
                request.setAttribute("INVALID", err);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
