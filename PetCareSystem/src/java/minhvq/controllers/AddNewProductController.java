/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import minhvq.daos.AccessoryDAO;
import minhvq.dtos.AccessoryDTO;
import minhvq.errorObjects.AccessoryErrorObj;

/**
 *
 * @author Quang Minh
 */
public class AddNewProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = "error.jsp";
        try {
            String id = request.getParameter("txtID");
            String name = request.getParameter("txtName");
            String price = request.getParameter("txtPrice");
            String quantity = request.getParameter("txtQuantity");
            String type = request.getParameter("type");
            String img = request.getParameter("fileImg");
            boolean valid = true;
            int quantityN = 0;
            float priceN = 0;
            int typeN = Integer.parseInt(type);
            AccessoryErrorObj errObj = new AccessoryErrorObj();
            if (name.length() == 0) {
                valid = false;
                errObj.setNameErr("Name can't be blank");
            }
            if (id.length() == 0) {
                valid = false;
                errObj.setIdErr("ID can't be blank");
            }
            if (price.length() == 0) {
                valid = false;
                errObj.setPriceErr("Price can't be blank");
            } else if (!price.matches("^\\d*\\.?\\d*$")) {
                valid = false;
                errObj.setPriceErr("Price is a number");
            } else {
                priceN = Float.parseFloat(price);
            }
            if (quantity.length() == 0) {
                valid = false;
                errObj.setQuantityErr("Quantity can't be blank");
            } else if (!quantity.matches("^[0-9]*")) {
                valid = false;
                errObj.setQuantityErr("Quantity is a number");
            } else {
                quantityN = Integer.parseInt(quantity);
            }
            System.out.println(img);
            System.out.println(img.substring(img.lastIndexOf("\\") + 1, img.length()));
            String file = img.substring(img.lastIndexOf("\\") + 1, img.length());

            AccessoryDTO dto = new AccessoryDTO(id, name, file, priceN, quantityN, typeN, false);
            if (valid) {
                AccessoryDAO dao = new AccessoryDAO();
                if (dao.insertAccessory(dto)) {
                    url = "ListAccessoryController";
                } else {
                    request.setAttribute("ERROR", "Insert failes");
                }
            } else {
                request.setAttribute("INVALID", errObj);
                url = "admin_InsertProduct.jsp";
            }
        } catch (Exception e) {
            if(e.getMessage().contains("duplicate")){
                url = "admin_InsertProduct.jsp";
                AccessoryErrorObj errObj = new AccessoryErrorObj();
                errObj.setIdErr("Product ID is already existed");
                request.setAttribute("INVALID", errObj);
            }
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
