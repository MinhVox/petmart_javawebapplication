/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Quang Minh
 */
public class MainController extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String LOGIN = "LoginController";
    private static final String REGISTER = "RegisterController";
    private static final String ADDCART = "AddToCartController";
    private static final String REMOVEITEM = "RemoveItemController";
    private static final String INCREASE = "IncreaseItemController";
    private static final String DECREASE = "DecreaseItemController";
    private static final String LOGOUT = "LogoutController";
    private static final String LOADITEM = "LoadItemController";
    private static final String ADDINVOICE = "AddInvoiceController";
    private static final String SEEACCOUNT = "ListAccountController";
    private static final String SEEPRODUCT = "ListAccessoryController";
    private static final String DELETEACC = "DeleteAccountController";
    private static final String DELETEPRO = "DeleteProductController";
    private static final String EDITPRO = "EditAccessoryController";
    private static final String UPDATEPRO = "UpdateAccessoryController";
    private static final String INSERTPRO = "AddNewProductController";
    private static final String SEEADMIN = "ListAdminController";
    private static final String INSERTADM = "AddNewAdminController";
    private static final String SEEORDER = "ListInvoiceController";
     private static final String ORDERDETAIL= "SeeOrderDetailController";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String action = request.getParameter("action");
            if (action.equals("Login")) {
                url = LOGIN;
            }
            if (action.equals("Register")) {
                url = REGISTER;
            }
            if (action.equalsIgnoreCase("Add To Cart")) {
                url = ADDCART;
            }
            if (action.equals("Remove Item")) {
                url = REMOVEITEM;
            }
            if (action.equals("+")) {
                url = INCREASE;
            }
            if (action.equals("-")) {
                url = DECREASE;
            }
            if (action.equals("logout")) {
                url = LOGOUT;
            }
            if (action.equals("INFO")) {
                url = LOADITEM;
            }
            if (action.equals("Add Invoice")) {
                url = ADDINVOICE;
            }
            if (action.equals("See Account")) {
                url = SEEACCOUNT;
            }
            if (action.equals("See Product")) {
                url = SEEPRODUCT;
            }
            if (action.equals("DeleteAcc")) {
                url = DELETEACC;
            }
            if (action.equals("DeletePro")) {
                url = DELETEPRO;
            }
            if (action.equals("EditPro")) {
                url = EDITPRO;
            }
            if (action.equals("UpdatePro")) {
                url = UPDATEPRO;
            }
            if (action.equals("InsertPro")) {
                url = INSERTPRO;
            }
            if (action.equals("See Admin")) {
                url = SEEADMIN;
            }
            if (action.equals("Add Admin")) {
                url = INSERTADM;
            }
            if (action.equals("See Invoice")) {
                url = SEEORDER;
            }if (action.equals("DETAIL")) {
                url = ORDERDETAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
