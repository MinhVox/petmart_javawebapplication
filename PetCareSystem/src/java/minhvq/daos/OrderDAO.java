/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import minhvq.connection.MyConnection;
import minhvq.dtos.AccessoryDTO;
import minhvq.dtos.OrderDTO;

/**
 *
 * @author Quang Minh
 */
public class OrderDAO implements Serializable {

    private static Connection con;
    private static PreparedStatement pre;
    private static ResultSet rs;

    public void closeConnection() throws Exception {
        if (rs != null) {
            rs.close();
        }
        if (pre != null) {
            pre.close();
        }
        if (con != null) {
            con.close();
        }
    }

    public int insertInvoice(String OwnerID, String address, String phone) throws Exception {
        int id = 0;
        try {
            String sql = "insert into Invoice_Product (CustomerID,Date,Phone,Address) values (?,?,?,?)";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pre.setString(1, OwnerID);
            pre.setDate(2, new java.sql.Date(System.currentTimeMillis()));
            pre.setString(3, phone);
            pre.setString(4, address);
            int i = pre.executeUpdate();
            rs = pre.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }

        } finally {
            closeConnection();
        }

        return id;
    }

    public boolean insertDetail(int id, String ProId, int quantity, float price) throws Exception {
        boolean result = false;
        try {
            String sql = "insert into Invoice_Product_Detail (IDinvoice_pro,IDpro,UnitPrice,Quantity) values (?,?,?,?)";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setInt(1, id);
            pre.setString(2, ProId);
            pre.setFloat(3, price);
            pre.setInt(4, quantity);
            result = pre.executeUpdate() > 0;
        } finally {
            closeConnection();
        }

        return result;
    }

    public OrderDTO orderINFO(int id) throws Exception {
        OrderDTO result = null;
        String  name =  null,
                phone = null,
                address = null;
        Date date = null;
        
        try {
            String sql = "Select  Date, Fullname , Phone ,Address  from Invoice_Product a,Account c where  a.CustomerID = c.Username and a.ID = ? ";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setInt(1, id);
            rs = pre.executeQuery();
            while (rs.next()) {
                date = rs.getDate("Date");
                name = rs.getString("Fullname");
                phone = rs.getString("Phone");
                address = rs.getString("Address");
                result = new OrderDTO(date, name, phone, address);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    public List<OrderDTO> listOrder() throws Exception {
        List<OrderDTO> result = null;
        String  name =  null,
                phone = null,
                address = null;
        int id;
        int total;
        Date date = null;
        OrderDTO dto;
        try {
            String sql = "Select ID, Date, Fullname , Phone ,Address, sum(d.Quantity*d.UnitPrice) as Total  from Invoice_Product a,Account c,Invoice_Product_Detail d where  a.CustomerID = c.Username and ID = d.IDinvoice_pro group by ID,Date, Fullname , Phone ,Address";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            rs = pre.executeQuery();
            result = new ArrayList<>();     
            while (rs.next()) {
                id = rs.getInt("ID");
                date = rs.getDate("Date");
                name = rs.getString("Fullname");
                phone = rs.getString("Phone");
                address = rs.getString("Address");
                total = rs.getInt("Total");
                dto = new OrderDTO(id, total, date, name, phone, address);
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    public List<OrderDTO> listOrderOfUser(String username) throws Exception {
        List<OrderDTO> result = null;
        String  name =  null,
                phone = null,
                address = null;
        int id,total;
        Date date = null;
        OrderDTO dto;
        try {
            String sql = "Select ID, Date, Fullname , Phone ,Address, sum(d.Quantity*d.UnitPrice) as Total  from Invoice_Product a,Account c,Invoice_Product_Detail d where  a.CustomerID = c.Username and ID = d.IDinvoice_pro and CustomerID = ? group by ID,Date, Fullname , Phone ,Address";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setString(1, username);
            rs = pre.executeQuery();         
            result = new ArrayList<>();     
            while (rs.next()) {
                id = rs.getInt("ID");
                date = rs.getDate("Date");
                name = rs.getString("Fullname");
                phone = rs.getString("Phone");
                address = rs.getString("Address");
                total = rs.getInt("Total");
                dto = new OrderDTO(id, total, date, name, phone, address);
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    public List<AccessoryDTO> listOrderDetail(int id) throws Exception{
        List<AccessoryDTO> result =null;
        String  pro = null,
                name =  null,
                img = null;
        int quantity;
        float price;
        AccessoryDTO dto;
        try {
            String sql = "select i.IDpro,Name,p.img,p.Price,i.Quantity from Product p, Invoice_Product_Detail i where p.IDpro = i.IDpro and IDinvoice_pro = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setInt(1, id);
            rs = pre.executeQuery();         
            result = new ArrayList<>();     
            while (rs.next()) {
                pro =rs.getString("IDpro");
                img = rs.getString("img");
                name = rs.getString("NAME");
                price = rs.getInt("Price");
                quantity = rs.getInt("Quantity");
                dto = new AccessoryDTO(img, name, img, price, quantity);
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }

}
