/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import minhvq.connection.MyConnection;
import minhvq.dtos.AccessoryDTO;

/**
 *
 * @author Quang Minh
 */
public class AccessoryDAO implements Serializable {

    private static Connection con;
    private static PreparedStatement pre;
    private static ResultSet rs;

    public void closeConnection() throws Exception {
        if (rs != null) {
            rs.close();
        }
        if (pre != null) {
            pre.close();
        }
        if (con != null) {
            con.close();
        }
    }

    public List<AccessoryDTO> listAccessories() throws Exception {
        List<AccessoryDTO> result = null;
        String id = null,
                name = null,
                img = null;
        float price;
        AccessoryDTO dto = null;
        try {
            String sql = "Select IDpro, Name, Price, img from Product where isDelete = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setBoolean(1, false);
            rs = pre.executeQuery();
            result = new ArrayList<>();
            while (rs.next()) {
                id = rs.getString("IDpro");
                name = rs.getString("Name");
                img = rs.getString("img");
                price = rs.getFloat("Price");
                dto = new AccessoryDTO(id, name, img, price);
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }

    public boolean insertAccessory(AccessoryDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Insert into Product (IDpro,Name,Price,Quantity,TypePet,img,isDelete) values (?,?,?,?,?,?,?)";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setString(1, dto.getId());
            pre.setString(2, dto.getName());
            pre.setFloat(3, dto.getPrice());
            pre.setInt(4, dto.getQuanity());
            pre.setInt(5, dto.getTypeID());
            pre.setString(6, dto.getImg());
            pre.setBoolean(7, false);
            check = pre.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }

    public boolean updateAccessory(AccessoryDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Update Product set Name = ?,Price = ?,Quantity = ?,TypePet = ? where IDpro = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setString(5, dto.getId());
            pre.setString(1, dto.getName());
            pre.setFloat(2, dto.getPrice());
            pre.setInt(3, dto.getQuanity());
            pre.setInt(4, dto.getTypeID());
            check = pre.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }

    public AccessoryDTO AccessoryInfo(String id) throws Exception {
        AccessoryDTO result = null;
        String name = null,
                img = null;
        float price;
        int quantity;
        try {
            String sql = "Select  Name, Price,Quantity, img from Product where isDelete = ? and  IDpro = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setBoolean(1, false);
            pre.setString(2, id);
            rs = pre.executeQuery();
            while (rs.next()) {
                quantity = rs.getInt("Quantity");
                name = rs.getString("Name");
                img = rs.getString("img");
                price = rs.getFloat("Price");
                result = new AccessoryDTO(id, name, img, price, quantity);
            }
        } finally {
            closeConnection();
        }
        return result;
    }

   

    public AccessoryDTO getAccessory(String id) throws Exception {
        AccessoryDTO result = null;
        String name = null,
                img = null;
        float price;
        try {
            String sql = "Select  Name, Price, img from Product where isDelete = ? and  IDpro = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setBoolean(1, false);
            pre.setString(2, id);
            rs = pre.executeQuery();
            while (rs.next()) {
                name = rs.getString("Name");
                img = rs.getString("img");
                price = rs.getFloat("Price");
                result = new AccessoryDTO(id, name, img, price);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    public int getQuantity(String id) throws Exception {
        int quantity = 0;
        try {
            String sql = "Select  Quantity  from Product where isDelete = ? and  IDpro = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setBoolean(1, false);
            pre.setString(2, id);
            rs = pre.executeQuery();
            while (rs.next()) {
                quantity = rs.getInt("Quantity");
            }
        } finally {
            closeConnection();
        }
        return quantity;
    }
    
    public List<AccessoryDTO> listAccessoriesFull() throws Exception {
        List<AccessoryDTO> result = null;
        String id = null,
                name = null,
                img = null,
                type = null;
        float price;
        int quantity;
        AccessoryDTO dto = null;
        try {
            String sql = "Select IDpro, Name, Price, img ,Quantity , TypeName from Product p , TypePet t where p.TypePet = t.TypeID and isDelete = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setBoolean(1, false);
            rs = pre.executeQuery();
            result = new ArrayList<>();
            while (rs.next()) {
                id = rs.getString("IDpro");
                name = rs.getString("Name");
                img = rs.getString("img");
                price = rs.getFloat("Price");
                type = rs.getString("TypeName");
                quantity = rs.getInt("Quantity");
                dto = new AccessoryDTO(id, name, img, price,quantity, type);
                result.add(dto);
            }
        } finally {
            closeConnection();
        }
        return result;
    }
    
    public boolean  deleteProduct(String id) throws Exception{
        boolean check = false;
        try{
            String sql = "Update Product set  isDelete = ? where IDpro = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setBoolean(1, true);
            pre.setString(2, id);
            check = pre.executeUpdate() > 0;
        }finally{
            closeConnection();
        }
        return check;
    }
    
    public AccessoryDTO findByPrimaryKey (String id) throws Exception{
        AccessoryDTO dto = null;
        try {
            String sql = "Select IDpro, Name, Price ,Quantity , TypePet from Product where IDpro = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setString(1, id);
            rs = pre.executeQuery();
            if(rs.next()){
                String name = rs.getString("Name");
                float price = rs.getFloat("Price");
                int quantity = rs.getInt("Quantity");
                int type = rs.getInt("TypePet");
                dto = new AccessoryDTO(id, name, price, quantity, type);
            }
        }finally{
            closeConnection();
        }
        return dto;
    }
}
