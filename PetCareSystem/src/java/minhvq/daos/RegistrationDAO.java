/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import minhvq.connection.MyConnection;
import minhvq.dtos.RegistrationDTO;

/**
 *
 * @author Quang Minh
 */
public class RegistrationDAO {
    private static Connection con;
    private static PreparedStatement pre;
    private static ResultSet rs;
    
    public void closeConnection() throws Exception{
        if(rs != null) rs.close();
        if(pre != null) pre.close();
        if(con != null) con.close();
    }
    
    public String checkLogin(String username, String password) throws Exception{
        String role = "failed";
        try {
           String sql = "Select Role from Account where username = ? and password = ?"; 
           con = MyConnection.getMyConnection();
           pre = con.prepareStatement(sql);
           pre.setString(1, username);
           pre.setString(2, password);
           rs = pre.executeQuery();
           while(rs.next()){
               role = rs.getString("Role");
           }
        }finally{
            closeConnection();
        }
        return role;
    }
    
    public String getFullname(String username) throws Exception{
        String name = null;
        try {
           String sql = "Select Fullname from Account where username = ? "; 
           con = MyConnection.getMyConnection();
           pre = con.prepareStatement(sql);
           pre.setString(1, username);
           rs = pre.executeQuery();
           while(rs.next()){
               name = rs.getString("Fullname");
           }
        }finally{
            closeConnection();
        }
        return name;
    }
    
    public boolean registerAccount(RegistrationDTO dto) throws Exception{
        boolean check = false;
        try {
            String sql = "Insert into Account(Username,Password,Fullname,Sex,Role,isDelete) values (?,?,?,?,?,?)";
            con =  MyConnection.getMyConnection();
            pre= con.prepareStatement(sql);
            pre.setString(1, dto.getUsername());
            pre.setString(2, dto.getPassword());
            pre.setString(3, dto.getFullname());
            pre.setString(4, dto.getSex());
            pre.setString(5, "user");
            pre.setBoolean(6, false);
            check = pre.executeUpdate() > 0 ;
        } finally{
            closeConnection();
        }
        return check;
    }
    
    public boolean addNewAdmin(RegistrationDTO dto) throws Exception{
        boolean check = false;
        try {
            String sql = "Insert into Account(Username,Password,Fullname,Sex,Role,isDelete) values (?,?,?,?,?,?)";
            con =  MyConnection.getMyConnection();
            pre= con.prepareStatement(sql);
            pre.setString(1, dto.getUsername());
            pre.setString(2, dto.getPassword());
            pre.setString(3, dto.getFullname());
            pre.setString(4, dto.getSex());
            pre.setString(5, "admin");
            pre.setBoolean(6, false);
            check = pre.executeUpdate() > 0 ;
        } finally{
            closeConnection();
        }
        return check;
    }
    
    public boolean  updateAccount(RegistrationDTO dto) throws Exception{
        boolean check = false;
        try {
            String sql = "Update Account set Passwork = ?,Fullname = ?, Sex = ? where Username = ?";
            con =  MyConnection.getMyConnection();
            pre= con.prepareStatement(sql);
            pre.setString(4, dto.getUsername());
            pre.setString(1, dto.getPassword());
            pre.setString(2, dto.getFullname());
            pre.setString(3, dto.getSex());
            check = pre.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }
    
     public List<RegistrationDTO> listAccount() throws Exception{
        String fullname = null,
                username = null,
                gender = null;
        List<RegistrationDTO> result = null;
        RegistrationDTO dto;
        try {
           String sql = "Select Fullname,Username,Sex from Account where Role = ? and isDelete = ?"; 
           con = MyConnection.getMyConnection();
           pre = con.prepareStatement(sql);
           pre.setString(1, "user");
           pre.setBoolean(2, false);
           rs = pre.executeQuery();
           result= new ArrayList<>();
           while(rs.next()){
              fullname = rs.getString("Fullname");
              username = rs.getString("Username");
              gender = rs.getString("Sex");
              dto = new RegistrationDTO(username, fullname,gender);
              result.add(dto);
           }
        }finally{
            closeConnection();
        }
        return result;
    }
    
      public List<RegistrationDTO> listAdmin() throws Exception{
        String fullname = null,
                username = null,
                gender = null;
        List<RegistrationDTO> result = null;
        RegistrationDTO dto;
        try {
           String sql = "Select Fullname,Username,Sex from Account where Role = ? and isDelete = ?"; 
           con = MyConnection.getMyConnection();
           pre = con.prepareStatement(sql);
           pre.setString(1, "admin");
           pre.setBoolean(2, false);
           rs = pre.executeQuery();
           result= new ArrayList<>();
           while(rs.next()){
              fullname = rs.getString("Fullname");
              username = rs.getString("Username");
              gender = rs.getString("Sex");
              dto = new RegistrationDTO(username, fullname,gender);
              result.add(dto);
           }
        }finally{
            closeConnection();
        }
        return result;
    }
     
     public boolean  deleteAccount(String id) throws Exception{
        boolean check = false;
        try{
            String sql = "Update Account set  isDelete = ? where username = ?";
            con = MyConnection.getMyConnection();
            pre = con.prepareStatement(sql);
            pre.setBoolean(1, true);
            pre.setString(2, id);
            check = pre.executeUpdate() > 0;
        }finally{
            closeConnection();
        }
        return check;
    }
}
