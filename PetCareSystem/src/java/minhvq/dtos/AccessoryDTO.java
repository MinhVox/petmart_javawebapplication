/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.dtos;

import java.io.Serializable;

/**
 *
 * @author Quang Minh
 */
public class AccessoryDTO implements Serializable {

    private String id, name, img,type;
    private Float price;
    private int quanity, typeID,sum;
    private boolean isDelete;

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public AccessoryDTO(String id, String name, String img, Float price, int quanity, int sum) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.price = price;
        this.quanity = quanity;
        this.sum = sum;
    }

    
    
    public AccessoryDTO(String id, String name, String img, Float price, int quanity) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.price = price;
        this.quanity = quanity;
    }

    public AccessoryDTO(String id, String name, Float price, int quanity, int typeID) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quanity = quanity;
        this.typeID = typeID;
    }

    public AccessoryDTO(String name, String img, Float price) {
        this.name = name;
        this.img = img;
        this.price = price;
    }

    public AccessoryDTO(String id, String name, String img, Float price, int quanity, int typeID, boolean isDelete) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.price = price;
        this.quanity = quanity;
        this.typeID = typeID;
        this.isDelete = isDelete;
    }
    
    public AccessoryDTO(String id, String name, String img, Float price, int quanity, String type) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.price = price;
        this.quanity = quanity;
        this.type = type;
    }

    public AccessoryDTO(String id, String name, String img, Float price) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.price = price;
    }

    public AccessoryDTO() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public int getQuanity() {
        return quanity;
    }

    public void setQuanity(int quanity) {
        this.quanity = quanity;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
