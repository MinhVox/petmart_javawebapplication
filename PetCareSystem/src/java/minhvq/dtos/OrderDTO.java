/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.dtos;

import java.io.Serializable;
import java.sql.Date;
import minhvq.daos.OrderDAO;

/**
 *
 * @author Quang Minh
 */
public class OrderDTO implements Serializable{
    private int id ,total;
    private Date Date;
    private String name,phone,address;

    public OrderDTO(int id, int total, Date Date, String name, String phone, String address) {
        this.id = id;
        this.total = total;
        this.Date = Date;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
 

    
    public OrderDTO(Date Date, String name, String phone, String address) {
        this.Date = Date;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    
    public OrderDTO() {
    }

    public OrderDTO(int id, Date Date, String name, String phone, String address) {
        this.id = id;
        this.Date = Date;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date Date) {
        this.Date = Date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
     
}
