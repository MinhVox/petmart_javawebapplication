/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.dtos;

import java.io.Serializable;

/**
 *
 * @author Quang Minh
 */
public class RegistrationDTO implements Serializable{
    private String role,username,fullname,password,sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public RegistrationDTO(String username, String fullname, String sex) {
        this.username = username;
        this.fullname = fullname;
        this.sex = sex;
    }

    public RegistrationDTO(String role, String username, String fullname, String password, String sex) {
        this.role = role;
        this.username = username;
        this.fullname = fullname;
        this.password = password;
        this.sex = sex;
    }

    public RegistrationDTO(String username, String fullname, String password, String sex) {
        this.username = username;
        this.fullname = fullname;
        this.password = password;
        this.sex = sex;
    }
    
    

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
