/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minhvq.dtos;

import java.io.Serializable;
import java.util.HashMap;
import minhvq.daos.AccessoryDAO;

/**
 *
 * @author Quang Minh
 */
public class ShoppingCartAccessory implements Serializable {

    private String customerName;
    private HashMap<String, AccessoryDTO> cart;

    public ShoppingCartAccessory(String customerName) {
        this.customerName = customerName;
        this.cart = new HashMap<>();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public HashMap<String, AccessoryDTO> getCart() {
        return cart;
    }

    public void addCart(AccessoryDTO dto) throws Exception {
        AccessoryDAO dao = new AccessoryDAO();
        if (this.cart.containsKey(dto.getId())) {
            int quanity = this.cart.get(dto.getId()).getQuanity() + 1;
            if (quanity > dao.getQuantity(dto.getId())) {  
                quanity = quanity - 1;
                dto.setQuanity(quanity);  
            }else{
                dto.setQuanity(quanity);    
                
            }            
        } 
        this.cart.put(dto.getId(), dto);
    }

    public void removeCart(String id) throws Exception {
        if (this.cart.containsKey(id)) {
            this.cart.remove(id);
        }
    }

    public void removeAll() throws Exception {
        cart.clear();
    }

    public float getTotal() throws Exception {
        float result = 0;
        for (AccessoryDTO dto : cart.values()) {
            result += dto.getPrice() * dto.getQuanity();
        }
        return result;
    }

    public int getValues() throws Exception {
        int result = 0;
        for (AccessoryDTO dto : cart.values()) {
            result += dto.getQuanity();
        }
        return result;
    }

    public void increaseQuantity(String id, int quanity) throws Exception {
        AccessoryDAO dao = new AccessoryDAO();
        if (this.cart.containsKey(id)) {
            if (quanity == dao.getQuantity(id)) {
                this.cart.get(id).setQuanity(quanity);
            } else {
                this.cart.get(id).setQuanity(quanity + 1);
            }
        }
    }

    public void decreaseQuantity(String id, int quanity) throws Exception {
        AccessoryDTO dto;
        if (this.cart.containsKey(id)) {
            if (cart.get(id).getQuanity() > 1) {
                cart.get(id).setQuanity(quanity - 1);
            } else {
                removeCart(id);
            }
        }
    }

}
