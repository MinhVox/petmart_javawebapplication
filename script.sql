USE [master]
GO
/****** Object:  Database [PetManager]    Script Date: 12/9/2019 10:03:35 PM ******/
CREATE DATABASE [PetManager]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PetManager', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PetManager.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PetManager_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PetManager_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PetManager] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PetManager].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PetManager] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PetManager] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PetManager] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PetManager] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PetManager] SET ARITHABORT OFF 
GO
ALTER DATABASE [PetManager] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PetManager] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PetManager] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PetManager] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PetManager] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PetManager] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PetManager] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PetManager] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PetManager] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PetManager] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PetManager] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PetManager] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PetManager] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PetManager] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PetManager] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PetManager] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PetManager] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PetManager] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PetManager] SET  MULTI_USER 
GO
ALTER DATABASE [PetManager] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PetManager] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PetManager] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PetManager] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PetManager] SET DELAYED_DURABILITY = DISABLED 
GO
USE [PetManager]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Username] [nvarchar](50) NOT NULL,
	[Fullname] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Sex] [nvarchar](10) NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
	[isDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Product]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [nvarchar](50) NOT NULL,
	[Phone] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](50) NOT NULL,
	[Date] [date] NOT NULL,
 CONSTRAINT [PK_Invoice_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Product_Detail]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Product_Detail](
	[IDinvoice_pro] [int] NOT NULL,
	[IDpro] [nvarchar](50) NOT NULL,
	[UnitPrice] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Invoice_Product_Detail] PRIMARY KEY CLUSTERED 
(
	[IDinvoice_pro] ASC,
	[IDpro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Service]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Service](
	[ID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Customer] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Invoice_Service] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice_Service_Detail]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice_Service_Detail](
	[ID_Invoice_Service] [int] NOT NULL,
	[PetID] [nvarchar](50) NOT NULL,
	[ID_Service] [nvarchar](50) NOT NULL,
	[DateDo] [datetime] NOT NULL,
 CONSTRAINT [PK_Invoice_Service_Detail] PRIMARY KEY CLUSTERED 
(
	[ID_Invoice_Service] ASC,
	[PetID] ASC,
	[ID_Service] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pet]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pet](
	[PetID] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Age] [int] NOT NULL,
	[Owner] [nvarchar](50) NOT NULL,
	[TypeID] [int] NOT NULL,
	[isDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Pet] PRIMARY KEY CLUSTERED 
(
	[PetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[IDpro] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Price] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[TypePet] [int] NOT NULL,
	[img] [nvarchar](50) NOT NULL,
	[isDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[IDpro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Service]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Service](
	[IDser] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Price] [int] NOT NULL,
	[TypeID] [int] NOT NULL,
	[Duration] [time](7) NOT NULL,
 CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED 
(
	[IDser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TypePet]    Script Date: 12/9/2019 10:03:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypePet](
	[TypeID] [int] NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TypePet] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'admin', N'Vo Quang Minh', N'1', N'Male', N'admin', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'admin1', N'Do Huong Thao', N'1', N'Femal', N'admin', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'admin3', N'a', N'1', N'Other', N'admin', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'admin4', N'b', N'1', N'Other', N'admin', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'admin5', N'c', N'1', N'Female', N'admin', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'alice', N'Ngoc Ha', N'1', N'Female', N'user', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'asuna', N'Do Huong Thao', N'1', N'Female', N'user', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'CuongLy', N'Ly Van Cuong', N'1', N'Male', N'user', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'KiriMiu', N'Minh Hung', N'1', N'Male', N'user', 0)
INSERT [dbo].[Account] ([Username], [Fullname], [Password], [Sex], [Role], [isDelete]) VALUES (N'kirito', N'Vo Quang Minh', N'1', N'Male', N'user', 0)
SET IDENTITY_INSERT [dbo].[Invoice_Product] ON 

INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (1, N'kirito', N'0903149606', N'79xvnt', CAST(N'2019-07-09' AS Date))
INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (2, N'kirito', N'0784445345', N'79xvnt', CAST(N'2019-07-10' AS Date))
INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (3, N'KiriMiu', N'123123123', N'25 To Ky', CAST(N'2019-07-11' AS Date))
INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (4, N'kirito', N'0903149606', N'79xvnt', CAST(N'2019-07-12' AS Date))
INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (5, N'alice', N'0903149606', N'39 Tran Dong', CAST(N'2019-07-12' AS Date))
INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (6, N'kirito', N'0784445345', N'79xvnt', CAST(N'2019-07-14' AS Date))
INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (7, N'kirimiu', N'0000000000', N'fhgfghfgh', CAST(N'2019-07-23' AS Date))
INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (8, N'kirito', N'00000000000', N'dfgdgfdfg', CAST(N'2019-07-23' AS Date))
INSERT [dbo].[Invoice_Product] ([ID], [CustomerID], [Phone], [Address], [Date]) VALUES (9, N'kirito', N'00000000', N'dtgdfgd', CAST(N'2019-07-23' AS Date))
SET IDENTITY_INSERT [dbo].[Invoice_Product] OFF
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (1, N'FD001', 60000, 3)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (2, N'FD003', 65000, 5)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (3, N'FD001', 60000, 2)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (3, N'FD004', 150000, 3)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (4, N'FD002', 60000, 5)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (5, N'FD004', 150000, 1)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (6, N'FD001', 50000, 1)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (6, N'FD002', 60000, 1)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (6, N'FD003', 65000, 1)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (6, N'FD004', 150000, 1)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (6, N'FD005', 15000, 1)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (7, N'FD001', 50000, 8)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (7, N'FD002', 60000, 8)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (8, N'FD001', 50000, 6)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (8, N'FD002', 60000, 5)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (9, N'FD001', 50000, 1)
INSERT [dbo].[Invoice_Product_Detail] ([IDinvoice_pro], [IDpro], [UnitPrice], [Quantity]) VALUES (9, N'FD002', 60000, 1)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'a', N'a', 10, 10, 1, N'FD006.jpg', 1)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FC001', N'Food for cat', 50000, 50, 2, N'FC001.jpg', 0)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FC002', N'Bone for Cat', 5000, 30, 2, N'FC002.jpg', 0)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FC004', N'Milk for Cat', 15000, 50, 2, N'FC003.jpg', 0)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FD001', N'Sausage for dog taste chicken', 50000, -5, 1, N'FD001.jpg', 0)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FD002', N'Bone chewed for dogs', 60000, -4, 1, N'FD002.jpg', 0)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FD003', N'Bone nutrition for dogs', 65000, 94, 1, N'FD003.jpg', 0)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FD004', N'Pate for dog', 150000, 95, 1, N'FD004.jpg', 0)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FD005', N'Bone reward for dogs', 15000, 99, 1, N'FD005.jpg', 0)
INSERT [dbo].[Product] ([IDpro], [Name], [Price], [Quantity], [TypePet], [img], [isDelete]) VALUES (N'FD006', N'Pedigree for dog', 50000, 5, 1, N'FD006.jpg', 0)
INSERT [dbo].[TypePet] ([TypeID], [TypeName]) VALUES (1, N'Dog')
INSERT [dbo].[TypePet] ([TypeID], [TypeName]) VALUES (2, N'Cat')
ALTER TABLE [dbo].[Invoice_Product]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Product_Account] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Account] ([Username])
GO
ALTER TABLE [dbo].[Invoice_Product] CHECK CONSTRAINT [FK_Invoice_Product_Account]
GO
ALTER TABLE [dbo].[Invoice_Product_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Product_Detail_Invoice_Product] FOREIGN KEY([IDinvoice_pro])
REFERENCES [dbo].[Invoice_Product] ([ID])
GO
ALTER TABLE [dbo].[Invoice_Product_Detail] CHECK CONSTRAINT [FK_Invoice_Product_Detail_Invoice_Product]
GO
ALTER TABLE [dbo].[Invoice_Product_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Product_Detail_Product] FOREIGN KEY([IDpro])
REFERENCES [dbo].[Product] ([IDpro])
GO
ALTER TABLE [dbo].[Invoice_Product_Detail] CHECK CONSTRAINT [FK_Invoice_Product_Detail_Product]
GO
ALTER TABLE [dbo].[Invoice_Service]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Service_Account] FOREIGN KEY([Customer])
REFERENCES [dbo].[Account] ([Username])
GO
ALTER TABLE [dbo].[Invoice_Service] CHECK CONSTRAINT [FK_Invoice_Service_Account]
GO
ALTER TABLE [dbo].[Invoice_Service_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Service_Detail_Invoice_Service] FOREIGN KEY([ID_Invoice_Service])
REFERENCES [dbo].[Invoice_Service] ([ID])
GO
ALTER TABLE [dbo].[Invoice_Service_Detail] CHECK CONSTRAINT [FK_Invoice_Service_Detail_Invoice_Service]
GO
ALTER TABLE [dbo].[Invoice_Service_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Service_Detail_Pet] FOREIGN KEY([PetID])
REFERENCES [dbo].[Pet] ([PetID])
GO
ALTER TABLE [dbo].[Invoice_Service_Detail] CHECK CONSTRAINT [FK_Invoice_Service_Detail_Pet]
GO
ALTER TABLE [dbo].[Invoice_Service_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_Service_Detail_Service] FOREIGN KEY([ID_Service])
REFERENCES [dbo].[Service] ([IDser])
GO
ALTER TABLE [dbo].[Invoice_Service_Detail] CHECK CONSTRAINT [FK_Invoice_Service_Detail_Service]
GO
ALTER TABLE [dbo].[Pet]  WITH CHECK ADD  CONSTRAINT [FK_Pet_Account] FOREIGN KEY([Owner])
REFERENCES [dbo].[Account] ([Username])
GO
ALTER TABLE [dbo].[Pet] CHECK CONSTRAINT [FK_Pet_Account]
GO
ALTER TABLE [dbo].[Pet]  WITH CHECK ADD  CONSTRAINT [FK_Pet_TypePet] FOREIGN KEY([TypeID])
REFERENCES [dbo].[TypePet] ([TypeID])
GO
ALTER TABLE [dbo].[Pet] CHECK CONSTRAINT [FK_Pet_TypePet]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_TypePet] FOREIGN KEY([TypePet])
REFERENCES [dbo].[TypePet] ([TypeID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_TypePet]
GO
ALTER TABLE [dbo].[Service]  WITH CHECK ADD  CONSTRAINT [FK_Service_TypePet] FOREIGN KEY([TypeID])
REFERENCES [dbo].[TypePet] ([TypeID])
GO
ALTER TABLE [dbo].[Service] CHECK CONSTRAINT [FK_Service_TypePet]
GO
USE [master]
GO
ALTER DATABASE [PetManager] SET  READ_WRITE 
GO
